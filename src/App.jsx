import React, { useState } from 'react'
import { Grid, GridColumn } from '@progress/kendo-react-grid'
import { process } from '@progress/kendo-data-query'
import styles from 'App.module.css'
import products from './products'
import '@progress/kendo-theme-default/dist/all.css'

function App() {
  products.map(product => {
    const _product = product
    _product.modified_date = new Date(product.modified_date)

    return _product
  })

  const dataGridState = {
    skip: 0,
    take: 10,
  }

  const [dataResult, setDataResult] = useState(process(products, dataGridState))
  const [dataState, setDataState] = useState(dataGridState)

  const dataStateChange = event => {
    setDataResult(process(products, event.data))
    setDataState(event.data)
  }

  const expandChange = event => {
    const isExpanded = event.dataItem.expanded === undefined ? event.dataItem.aggregates : event.dataItem.expanded

    const _event = event
    _event.dataItem.expanded = !isExpanded

    setDataState({ ...dataState })

    return _event
  }

  return (
    <div>
      <h1 className={styles.title}>SPA Kendo React</h1>
      <Grid
        style={{ height: '505px' }}
        expandField="expanded"
        sortable
        filterable
        groupable
        reorderable
        sort={dataState.sort}
        take={dataState.take}
        group={dataState.group}
        skip={dataState.skip}
        filter={dataState.filter}
        data={dataResult}
        onExpandChange={expandChange}
        onDataStateChange={dataStateChange}
        pageable={{ buttonCount: 4, pageSizes: true }}
      >
        <GridColumn field="order_id" title="Order ID" filterable={false} width="100px" />
        <GridColumn field="modified_date" title="Modified date" format="{0:dd.MM.yyyy}" filter="date" width="240px" />
        <GridColumn field="amount" title="Amount" format="{0:c}" filter="numeric" width="200px" />
        <GridColumn field="shipment_country" title="Shipment country" filterable={false} width="200px" />
        <GridColumn field="description" title="Description" filterable={false} />
      </Grid>

      <p className={styles.copyright}>Tested app for Lab. Kaspersky by Eduard Chagirov.</p>
    </div>
  )
}

export default App
